import server from './server';
import messageProcessor from './messageProcessor';

server.on('message', async (message) => {
  if (message.content.includes('dj'))
    messageProcessor.processActionDJFromDiscord(message);
});
