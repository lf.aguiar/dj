import ytdl, { filterFormats } from 'ytdl-core';
class djAction {
  playMusic(connection, message, comands) {
    //verifica se tem argumentos; senao tiver toca o default
    if (!comands.argument1) return this.playStreamDefault(connection);

    //verifica se a url passada e um link do youtube se for, ele tenta tocar, senao toca a default
    let youtubeUrlString = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

    if (youtubeUrlString.test(comands.argument1)) {
      return this.playYoutubeUrl(connection, message, comands);
      // return this.playStreamDefault(connection);
    } else {
      message.channel.send(
        'Não consegui tocar essa tua música. Toma ai um batidão louco'
      );
      return this.playStreamDefault(connection);
    }
  }

  async playYoutubeUrl(connection, message, commands) {
    console.log(commands);
    connection.play(await ytdl(commands.argument1), {
      filterFormats: 'audioonly',
    });
  }

  playStreamDefault(connection, message, comands) {
    connection.play('http://s04.radio-tochka.com:5470/mount');
  }

  stopMusic(connection, message, comands) {
    connection.disconnect();
  }

  needHelp(connection, message, comands) {
    message.channel.send(
      `
            .██╗      ██╗ ███████╗ ██╗                 ██████╗    ██╗
 ██║      ██║ ██╔════╝ ██║                 ██╔══██╗ ██║
 ███████║ █████╗       ██║                 ██████╔╝ ██║
 ██╔═   ██║ ██╔══╝       ██║                 ██╔═══╝    ╚═╝
 ██║      ██║ ███████╗ ███████╗ ██║                  ██╗
 ╚═╝      ╚═╝ ╚══════╝ ╚══════╝ ╚═╝                  ╚═╝

comando 'dj' deve ser colocado na frente de qualquer acao.

Ex:
_dj play https://www.youtube.com/watch?v=dQw4w9WgXcQ_
            
acao                        |                descricao
--------------------+------------------------------------------------------------------
**help**---------------| mostra todos os comandos que o bot consegue fazer        |
**play**---------------| toca algum streaming ou musica default                                |
**play _link _**----------| toca o link do youtube escolhido                                              |
**quit**--------------- | manda o bot ir catar coquinho                                                  |
----------------------------------------------------------------------------------------`
    );
  }
}

export default new djAction();
