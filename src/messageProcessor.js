import djAction from './actions/djAction';

class Actions {
  async processActionDJFromDiscord(message) {
    const { content } = message;
    const comands = await this.processArguments(content);

    console.log(message.member.voice);

    const connection = await message.member.voice.channel.join();

    switch (comands.action) {
      case 'play': {
        djAction.playMusic(connection, message, comands);
        break;
      }
      case 'quit': {
        djAction.stopMusic(connection, message, comands);
        break;
      }
      case 'help': {
        djAction.needHelp(connection, message, comands);
        break;
      }
      case 'kill': {
        message.reply(`Tá achando que eu sou o Bolsonaro pra matar os outros?`);
        break;
      }

      default: {
        message.reply(
          `Não reconheço o comando '${comands.action}'. I'm a joke to you?`
        );
      }
    }
  }

  async processArguments(args) {
    // args.toLowerCase();
    const argsArray = args.split(' ');

    const commands = {
      principal: argsArray[0],
      action: argsArray[1],
      argument1: argsArray[2],
    };
    return commands;
  }
}
export default new Actions();
